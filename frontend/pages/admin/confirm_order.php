<?php include 'headadmin.php'; ?>

<?php include 'header.php'; ?>

<?php include 'menu.php';  ?>

<script>
    function confirm(id,type) {
        
        document.location = "../../../api/confirm.php?id="+id+"&act=confirm&type="+type;
        
    }
</script>


<!-- <form > -->
    <div class="main_content_iner_co">
        <h1 style="margin: 5px 20px">Comfirm Order</h1>
    </div>
    <h2 style="margin: 12px 0 0 50px">Member</h2>
    <div class="list_order">
        <div class="table_section">
            
        <?php 
        $q_mem = "SELECT NO
        FROM RECEIPT_MEMBER
        WHERE CONFIRM = 0";
        $run_mem = oci_parse($conn, $q_mem);
        oci_execute($run_mem);
    
        $rec_mem_id = array();
        while($row = oci_fetch_array($run_mem)){
            array_push($rec_mem_id, $row['NO']);
        }    
        foreach($rec_mem_id as $id){
            $q_mem = "SELECT 
            RECEIPT_MEMBER.NO,
            RECEIPT_LIST_M.NO as nolist,
            RECEIPT_LIST_M.AMOUNT,
            RECEIPT_LIST_M.TOTAL as price,
            RECEIPT_MEMBER.TOTAL,
            RECEIPT_MEMBER.R_DATE,
            PRODUCT_NAME.NAME as pname,
            M_NAME,
            SIZETABLE.NAME as sname,
            COLORTABLE.NAME as cname
            FROM RECEIPT_MEMBER, RECEIPT_LIST_M, PRODUCT_NAME, SIZETABLE, COLORTABLE, MEMBERS
            WHERE (RECEIPT_LIST_M.NOREC = RECEIPT_MEMBER.NO AND
            PRODUCT_NAME.ID = RECEIPT_LIST_M.ID_PRODUCT AND
            SIZETABLE.ID = RECEIPT_LIST_M.ID_SIZE AND 
            COLORTABLE.ID = RECEIPT_LIST_M.ID_COLOR AND
            MEMBERS.M_ID = RECEIPT_MEMBER.M_ID) AND
            CONFIRM = 0 AND RECEIPT_MEMBER.NO = ".$id."
            ORDER BY RECEIPT_MEMBER.NO ASC"
            ;
            $run_mem = oci_parse($conn, $q_mem);
            oci_execute($run_mem);
            $run_mem_name = oci_parse($conn, $q_mem);
            oci_execute($run_mem_name);
        
        ?>
            <?php $name = oci_fetch_array($run_mem_name)?>
            <table>
                <thead>
                    <tr>
                        <th class="title_code">No</th>
                        <th class="title_code"><?= $id?></th>
                        <th class="title_code"><?= $name['M_NAME']?></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Images</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>color</th>
                        <th>size</th>
                        <th>Count</th>
                        <th>
                            <button onclick="confirm(<?= $id ?>,'member')"
                                style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
                                ยืนยัน
                            </button>
                            <button
                                style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff;cursor: pointer;">
                                ยกเลิก
                            </button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php while($row = oci_fetch_array($run_mem)){  $total = $row['TOTAL'] ?>
                    <tr> 
                        <td><?= $row['NOLIST'] ?></td>
                        <td><img src="images/shop/" alt="รูป"></td>
                        <td><?= $row['PNAME'] ?></td>
                        <td><?= $row['PRICE'] ?> THB</td>
                        <td><?= $row['CNAME'] ?></td>
                        <td><?= $row['SNAME'] ?></td>
                        <td><?= $row['AMOUNT'] ?></td>
                    </tr>
                    <?php }?>
                    
                    <tr>
                        <td>Total price</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?= $total ?> THB</td>
                    </tr>
                </tbody>
            </table>
        <?php }?>                
        </div>
    </div>

    <!---------------------------------------------------------------------->

    <div class="list_order">
        <div class="table_section">
        <h2 style="margin: 0px 0 0 25px">Customer</h2>                
        <?php 
        $q = "SELECT NO
        FROM RECEIPT
        WHERE CONFIRM = 0";
        $run = oci_parse($conn, $q);
        oci_execute($run);
    
        $rec_id = array();
        while($row = oci_fetch_array($run)){
            array_push($rec_id, $row['NO']);
        }    
        foreach($rec_id as $id){
            $q = "SELECT 
            RECEIPT.NO,
            RECEIPT_LIST.NO as nolist,
            RECEIPT_LIST.AMOUNT,
            RECEIPT_LIST.TOTAL as price,
            RECEIPT.TOTAL,
            CUSTOMERS.C_NAME,
            RECEIPT.R_DATE,
            PRODUCT_NAME.NAME as pname,
            SIZETABLE.NAME as sname,
            COLORTABLE.NAME as cname
            FROM RECEIPT, RECEIPT_LIST, PRODUCT_NAME, SIZETABLE, COLORTABLE, CUSTOMERS
            WHERE (RECEIPT_LIST.NOREC = RECEIPT.NO AND
            PRODUCT_NAME.ID = RECEIPT_LIST.ID_PRODUCT AND
            SIZETABLE.ID = RECEIPT_LIST.ID_SIZE AND 
            COLORTABLE.ID = RECEIPT_LIST.ID_COLOR AND
            CUSTOMERS.C_NAME = RECEIPT.C_NAME ) AND
            CONFIRM = 0 AND RECEIPT.NO = ".$id."
            ORDER BY RECEIPT.NO ASC"
            ;
            $run = oci_parse($conn, $q);
            oci_execute($run);
            $run_name = oci_parse($conn, $q);
            oci_execute($run_name);
        
        ?>
            <?php $name = oci_fetch_array($run_name)?>

        
            <table>
                <thead>
                    <tr>
                        <th class="title_code">No</th>
                        <th class="title_code"><?= $id?></th>
                        <th class="title_code"><?= $name['C_NAME']?></th>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Images</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>color</th>
                        <th>size</th>
                        <th>Amount</th>
                        <th>
                            <button onclick="confirm(<?= $id ?>,'customer')"
                                style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
                                ยืนยัน
                            </button>
                            <button
                                style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff;cursor: pointer;">
                                ยกเลิก
                            </button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php while($row = oci_fetch_array($run)){  $total = $row['TOTAL'] ?>
                    <tr> 
                        <td><?= $row['NOLIST'] ?></td>
                        <td><img src="images/shop/" alt="รูป"></td>
                        <td><?= $row['PNAME'] ?></td>
                        <td><?= $row['PRICE'] ?> THB</td>
                        <td><?= $row['CNAME'] ?></td>
                        <td><?= $row['SNAME'] ?></td>
                        <td><?= $row['AMOUNT'] ?></td>
                    </tr>
                    <?php }?>
                    <tr>
                        <td>Total price</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?= $total ?> THB</td>
                    </tr>
                </tbody>
            </table>
            <?php }?>
        </div>
         
    </div>

<!-- </form> -->