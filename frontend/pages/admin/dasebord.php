
    <section class="main_content dashboard_part">
        <div class="container-fluid g-0">
            <div class="row">
                <div class="col-lg-12 p-0">
                    <div class="header_iner d-flex justify-content-between align-items-center">
                        <!--search-->
                        <div class="serach_field-area">
                            <div class="search_inner">
                                <form action="#">
                                    <div class="search_field">
                                        <input type="text" placeholder="Search here...">
                                        <button type="submit"> <img src="img/icon/icon_search.svg" alt="#"> </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--Logout-->
                        <div class="header_right d-flex justify-content-between align-items-center">
                            <div class="header_notification_warp d-flex align-items-center">
                                <li><a href="#"> <img src="img/icon/bell.svg" alt="กระดิ่ง"> </a></li>
                                <li><a href="#"> <img src="img/icon/msg.svg" alt="ข้อความ"> </a></li>
                            </div>
                            <div class="profile_info">
                                <img src="img/client_img.png" alt="รูปโลโก้">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_content_iner">
            <div class="container-fluid p-0">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="single_element">
                            <div class="quick_activity">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="quick_activity_wrap">
                                            <div class="single_quick_activity d-flex">
                                                <div class="icon"></div>
                                                <div class="count_content">
                                                    <h3><span class="counter">1</span></h3>
                                                    <p>Member</p>
                                                </div>
                                            </div>
                                            <!---->
                                            <div class="single_quick_activity d-flex">
                                                <div class="icon"></div>
                                                <div class="count_content">
                                                    <h3><span class="counter">10</span></h3>
                                                    <p>Employee</p>
                                                </div>
                                            </div>
                                            <!---->
                                            <div class="single_quick_activity d-flex">
                                                <div class="icon"></div>
                                                <div class="count_content">
                                                    <h3><span class="counter">100</span></h3>
                                                    <p>Sold out</p>
                                                </div>
                                            </div>
                                            <!---->
                                            <div class="single_quick_activity d-flex">
                                                <div class="icon"></div>
                                                <div class="count_content">
                                                    <h3><span class="counter">1000</span></h3>
                                                    <p>Inventories</p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---Details--->
        <div class="main_content_detail">
            <div class="container-fluid p-0">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="single_element_detail">
                            <div class="quick_activity_detail">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="quick_activity_wrap_detail">

                                            <form action="">
                                                <div class="single_quick_activity_detail">
                                                    <div class="count_content_detail">
                                                        <div class="test">
                                                            <h3><span class="counter">Sold out List</span></h3>
                                                            <div>
                                                                <input type="date">
                                                                <span> - </span> 
                                                                <input type="date">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <hr>
                                                        <div class="table_section">
                                                            <table>
                                                                <thead>
                                                                    <tr>
                                                                        <th>No</th>
                                                                        <th>Images</th>
                                                                        <th>Name</th>
                                                                        <th>Price</th>
                                                                        <th>Date</th>
                                                                        <th>Count</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td><img src="images/shop/kkk.png" alt="รูป"></td>
                                                                        <td>Shope</td>
                                                                        <td>599 THB</td>
                                                                        <td>13/05/2562</td>
                                                                        <td>3</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                            <!---->
                                            <form action="">
                                                <div class="single_quick_activity_detail">
                                                    <div class="count_content">
                                                        <div class="test">
                                                            <h3><span class="counter">Inventories List</span></h3>
                                                            <div>
                                                                <input type="date">
                                                                <span> - </span> 
                                                                <input type="date">
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="table_section">
                                                            <table>
                                                                <thead>
                                                                    <tr>
                                                                        <th>No</th>
                                                                        <th>Images</th>
                                                                        <th>Name</th>
                                                                        <th>Price</th>
                                                                        <th>Date</th>
                                                                        <th>Count</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td><img src="images/shop/kkk.png" alt="รูป"></td>
                                                                        <td>Shope</td>
                                                                        <td>599 THB</td>
                                                                        <td>13/05/2562</td>
                                                                        <td>3</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main_content_bestseller">
            <div class="container-fluid p-0">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="single_element_bestseller">
                            <div class="quick_activity_bestseller">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="quick_activity_wrap_bestseller">

                                            <form action="">
                                                <div class="single_quick_activity_bestseller">
                                                    <div class="count_content_bestseller">
                                                        <div class="test">
                                                            <h3><span class="counter">Best seller</span></h3>
                                                            <div>
                                                                <input type="date">
                                                                <span> - </span> 
                                                                <input type="date">
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="table_section">
                                                            <table>
                                                                <thead>
                                                                    <tr>
                                                                        <th>No</th>
                                                                        <th>Images</th>
                                                                        <th>Name</th>
                                                                        <th>Color</th>
                                                                        <th>Size</th>
                                                                        <th>Price</th>
                                                                        <th>Date</th>
                                                                        <th>Count</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td><img src="images/shop/kkk.png" alt="รูป"></td>
                                                                        <td>Shope</td>
                                                                        <td>Red</td>
                                                                        <td>XL</td>
                                                                        <td>599 THB</td>
                                                                        <td>13/05/2562</td>
                                                                        <td>3</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
