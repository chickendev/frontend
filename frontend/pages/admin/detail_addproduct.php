
<div class="modal_add" id="modal_addname" style="display: none;">
    
    <div class="modall-page">
    <h1>เพิ่มชื่อสินค้า</h1>
        <form action="../admin/manageproduct.php" method="POST">
            <div class="mr-4">
                <span>ชื่อ : </span><br><input type="text" name="namePN" required><br>
            </div>
            <!-- onclick="window.location='billing.php'" -->
            <input type="submit" value="OK" name="nameP"                                style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <input type="submit" value="Cancel" onclick="closeModal()" style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <!-- <a href="billing.php"></a> -->
        </form>
    </div>
</div>

<div class="modal_add" id="modal_addyeeho" style="display: none;">
    
    <div class="modall-page">
    <h1>เพิ่มยี่ห้อ</h1>
        <form action="../admin/manageproduct.php" method="POST">
            <div class="mr-4">
                <span>ชื่อ : </span><br><input type="text" name="brandPN" required><br>
            </div>
            <!-- onclick="window.location='billing.php'" -->
            <input type="submit" value="OK"  name="brandP"                               style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <input type="submit" value="Cancel" onclick="closeModal()" style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <!-- <a href="billing.php"></a> -->
        </form>
    </div>
</div>

<div class="modal_add" id="modal_addtype" style="display: none;">
    
    <div class="modall-page">
    <h1>เพิ่มชื่อประเภท</h1>
        <form action="../admin/manageproduct.php" method="POST">
            <div class="mr-4">
                <span>ชื่อ : </span><br><input type="text" name="typePN" required><br>
            </div>
            <!-- onclick="window.location='billing.php'" -->
            <input type="submit" value="OK"  name="typeP"                               style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <input type="submit" value="Cancel" onclick="closeModal()" style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <!-- <a href="billing.php"></a> -->
        </form>
    </div>
</div>

<div class="modal_add" id="modal_addcolor" style="display: none;">
    
    <div class="modall-page">
    <h1>เพิ่มชื่อสี</h1>
        <form action="../admin/manageproduct.php" method="POST">
            <div class="mr-4">
                <span>ชื่อสี : </span><br><input type="text" name="colorPN" required><br>
            </div>
            <div class="mr-4">
                <span>Code สี : </span><br><input type="text" name="colorPC" required><br>
            </div>
            <!-- onclick="window.location='billing.php'" -->
            <input type="submit" value="OK"  name="colorP"                               style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <input type="submit" value="Cancel" onclick="closeModal()" style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <!-- <a href="billing.php"></a> -->
        </form>
    </div>
</div>

<div class="modal_add" id="modal_addseason" style="display: none;">
    
    <div class="modall-page">
    <h1>เพิ่มฤดูกาล</h1>
        <form action="../admin/manageproduct.php" method="POST">
            <div class="mr-4">
                <span>ฤดูกาล : </span><br><input type="text" name="seasonPN" required><br>
            </div>
            <!-- onclick="window.location='billing.php'" -->
            <input type="submit" value="OK"    name="seasonP"                             style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <input type="submit" value="Cancel" onclick="closeModal()" style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <!-- <a href="billing.php"></a> -->
        </form>
    </div>
</div>

<div class="modal_add" id="modal_addsizee" style="display: none;">
    
    <div class="modall-page">
    <h1>เพิ่ม Size</h1>
        <form action="../admin/manageproduct.php" method="POST">
            <div class="mr-4">
                <span>Size : </span><br><input type="text" name="sizePN" required><br>
            </div>
            <!-- onclick="window.location='billing.php'" -->
            <input type="submit" value="OK"  name="sizeP"                style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <input type="submit" value="Cancel" onclick="closeModal()" style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
            <!-- <a href="billing.php"></a> -->
        </form>
    </div>
</div>

<script>
function openaddnamePro() {
    $('#modal_addname').css('display', 'flex')
}

function openaddyeeho() {
    $('#modal_addyeeho').css('display', 'flex')
}
function openaddtype() {
    $('#modal_addtype').css('display', 'flex')
}
function openaddcolor() {
    $('#modal_addcolor').css('display', 'flex')
}
function openaddseason() {
    $('#modal_addseason').css('display', 'flex')
}
function openaddsizee() {
    $('#modal_addsizee').css('display', 'flex')
}

function closeModal() {
    $(".modal_add").css('display', 'none')
}
</script>