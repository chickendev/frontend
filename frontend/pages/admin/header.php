<section class="main_content dashboard_part">
    <div class="container-fluid g-0">
        <div class="row">
            <div class="col-lg-12 p-0">
                <div class="header_iner d-flex justify-content-between align-items-center">
                    <!--search-->
                    <div class="serach_field-area">
                        <div class="search_inner">
                            <form action="#">
                                <div class="search_field">
                                    <input type="text" placeholder="Search here...">
                                    <button type="submit"> <img src="../../images/icon/icon-search.png" style="width: 15px" alt="#"> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--Logout-->
                    <div class="header_right d-flex justify-content-between align-items-center">
                        <div class="header_notification_warp d-flex align-items-center">
                            
                        </div>
                        <div class="profile_info">
                        <img src="../../images/icon/icon-logout.png" alt="รูปโลโก้" onclick="window.location='../system/logout.php'">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>