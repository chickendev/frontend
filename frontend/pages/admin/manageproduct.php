<?php include 'headadmin.php'; ?>

<?php include 'header.php'; ?>

<?php include 'menu.php'; ?>

<div class="main_content_bestseller">
<?php 
    
    $inventory = oci_parse($conn, 
    "SELECT ID_PRODUCT as PID, 
    ID_SIZE as SID, 
    ID_COLOR as CID,
    PRODUCT_NAME.NAME as PNAME,
    SIZETABLE.NAME as SNAME,
    SEASON.NAME as SEASON, 
    COLORTABLE.NAME as CNAME, 
    PHOTO, 
    PRICE
    FROM PRODUCT_LIST, 
    PRODUCT_NAME, 
    SIZETABLE, 
    SEASON,
    COLORTABLE
    WHERE (
    ID_PRODUCT = PRODUCT_NAME.ID AND
    ID_SIZE = SIZETABLE.ID AND
    ID_SEA = SEASON.ID AND
    ID_COLOR = COLORTABLE.ID
    )");
    oci_execute($inventory);
    $invID = 1;       
    
    $countmem = oci_parse($conn, "SELECT count(*)
    FROM MEMBERS");
    ociexecute($countmem);
    $members = oci_fetch_array($countmem);
    
    $countemp = oci_parse($conn, "SELECT count(*)
    FROM EMPLOYEE");
    ociexecute($countemp);
    $emp = oci_fetch_array($countemp);

    
    $countinv = oci_parse($conn, "SELECT SUM(AMOUNT)
    FROM PRODUCT_DETAIL");
    ociexecute($countinv);
    $inv = oci_fetch_array($countinv);    
    
      

?>    
<?php 
if(isset($_POST['SubmitProduct'])){
    $name = $_POST['name'];
    $brand = $_POST['brand'];
    $type = $_POST['type']; 
    $color = $_POST['color'];
    $season = $_POST['season'];
    $sizePro = $_POST['sizePro'];
    $prizebuy = $_POST['prizebuy'];
    $prizesell = $_POST['prizesell'];
    $amout = $_POST['amout'];
    $img = $_POST['img'];
    

    $product = oci_parse($conn, "SELECT ID FROM PRODUCT WHERE ID_BRAND = ".$brand." AND ID_PD_NAME = ".$name." AND ID_TYPE = ".$type);
    oci_execute( $product);
    $proD = oci_fetch_array( $product);
    
    if(isset($proD[0])){
        
        $proid = $proD['ID'];
        
    }else{
        $countidp = oci_parse($conn, "SELECT count(*) 
            FROM PRODUCT");
            oci_execute($countidp);
            $row = oci_fetch_array($countidp);
            $idp = $row['COUNT(*)'];
            $idp++;
            $update = oci_parse($conn, 
            "INSERT INTO PRODUCT (ID,ID_BRAND,ID_PD_NAME,ID_TYPE)
                            VALUES ('".$idp."','".$brand."','".$name."','".$type."')");
            oci_execute($update);

            $proid = $idp;
            
    }

    $product = oci_parse($conn, "SELECT ID_SEA FROM PRODUCT_LIST WHERE ID_PRODUCT = ".$proid." AND ID_SIZE = ".$sizePro." AND ID_COLOR = ".$color);
    oci_execute( $product);
    $proD = oci_fetch_array( $product);
    
    if(isset($proD[0])){
        if($proD['ID_SEA'] != $season){
        echo "<script>alert('รายการสินค้านี้มี Season แล้ว กรุณาเปลี่ยนชื่อสินค้า');</script>";
        echo '<script>window.location.href = "../admin/manageproduct.php";</script>';

        }else{
            $check = 1 ;
        }
        
    }else{
        $countidp = oci_parse($conn, "SELECT count(*) 
            FROM PRODUCT_LIST");
            oci_execute($countidp);
            $row = oci_fetch_array($countidp);
            $idp = $row['COUNT(*)'];
            $idp++;
            $update = oci_parse($conn, 
            "INSERT INTO PRODUCT_LIST (ID_PRODUCT,ID_SIZE,ID_COLOR,ID_SEA,PRICE,PHOTO)
                            VALUES ('".$proid."','".$sizePro."','".$color."','".$season."','".$prizesell."','".$img."')");
            oci_execute($update);
            $check = 1 ;          
    }
    
    if( $check == 1){
    $product = oci_parse($conn, "SELECT count(*) FROM PRODUCT_DETAIL WHERE ID_PRODUCT = ".$proid." AND ID_SIZE = ".$sizePro." AND ID_COLOR = ".$color);
    oci_execute( $product);
    $proD = oci_fetch_array( $product);
    $nolot = $proD['COUNT(*)'];
    $nolot++;
    $update = oci_parse($conn, 
            "INSERT INTO PRODUCT_DETAIL (NOLOT,ID_PRODUCT,ID_SIZE,ID_COLOR,AMOUNT,COST)
                            VALUES ('".$nolot."','".$proid."','".$sizePro."','".$color."','".$amout."','".$prizebuy."')");
            oci_execute($update);
            echo "<script>alert('เพิ่มสินค้าสำเร็จ');</script>";
    }else{
        echo "<script>alert('เกิดข้อผิดพลาด');</script>";
    }

    

}





?>
    <div class="container-fluid p-0">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="single_element_bestseller">
                    <div class="quick_activity_bestseller">
                        <div class="row">
                            <div class="col-12">
                                <div class="quick_activity_wrap_bestseller">

                                <form action="../admin/manageproduct.php" method="post">
                                        <div class="single_quick_activity_bestseller">
                                            <div class="count_content_bestseller">
                                            <div class="test">
                                                    <h3><span class="counter">Manage product</span></h3>
                                                </div>
                                                <hr>
                                                 <div class="set_manageadmin">
                                                    <div>
                                                        <img id="blah" src="../../images/icon/plus.png"
                                                            alt="your image" /><br>
                                                        <input name="img" type='file' onchange="readURL(this);" />
                                                    </div>
                                                    
                                                    <div>
                                                        <div class="set_manageadmin2">
                                                            <label for="">ชื่อสินค้า : </label><br><select name="name" id="">
                                                            <?php 
                                                                 $pro = oci_parse($conn, "SELECT NAME,ID FROM PRODUCT_NAME ORDER BY ID ASC");
                                                                 oci_execute($pro);
                                                                 while ($row = oci_fetch_array($pro)) {
                                                                    echo '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
                                                                 }
                                                                ?>

                                                            </select>
                                                            
                                                        </div>
                                                        <div class="set_manageadmin2">
                                                            <label for="">ยี่ห้อ : </label><br><select name="brand" id="">
                                                            <?php 
                                                                 $pro = oci_parse($conn, "SELECT NAME,ID FROM BRAND ORDER BY ID ASC");
                                                                 oci_execute($pro);
                                                                 while ($row = oci_fetch_array($pro)) {
                                                                    echo '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
                                                                 }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="set_manageadmin2">
                                                            <label for="">ประเภท : </label><br><select name="type" id="">
                                                            <?php 
                                                                 $pro = oci_parse($conn, "SELECT NAME,ID FROM TYPE ORDER BY ID ASC");
                                                                 oci_execute($pro);
                                                                 while ($row = oci_fetch_array($pro)) {
                                                                    echo '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
                                                                 }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="set_manageadmin2">
                                                            <label for="">สี : </label><br><select name="color" id="">
                                                            <?php 
                                                                 $pro = oci_parse($conn, "SELECT NAME,ID FROM COLORTABLE ORDER BY ID ASC");
                                                                 oci_execute($pro);
                                                                 while ($row = oci_fetch_array($pro)) {
                                                                    echo '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
                                                                 }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="set_manageadmin2">
                                                            <label for="">ฤดูกาล : </label><br><select name="season" id="">
                                                            <?php 
                                                                 $pro = oci_parse($conn, "SELECT NAME,ID FROM SEASON ORDER BY ID ASC");
                                                                 oci_execute($pro);
                                                                 while ($row = oci_fetch_array($pro)) {
                                                                    echo '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
                                                                 }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        
                                                        
                                                      
                                                    </div>

                                                    <div>
                                                    
                                                    <div class="set_manageadmin2">
                                                            <label for="">Size : </label><br><select name="sizePro" id="">
                                                                <?php 
                                                                 $pro = oci_parse($conn, "SELECT NAME,ID FROM SIZETABLE ORDER BY ID ASC");
                                                                 oci_execute($pro);
                                                                 while ($row = oci_fetch_array($pro)) {
                                                                    echo '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
                                                                 }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="set_manageadmin2">
                                                            <label for="">จำนวน : </label><br><input type="number" name="amout"  required><br>
                                                        </div>
                                                        
                                                        <div class="set_manageadmin2">
                                                            <label for="">ราคาทุน : </label><br><input type="number" name="prizebuy"  required ><br>
                                                        </div>

                                                        <div class="set_manageadmin2">
                                                            <label for="">ราคาขาย : </label><br><input type="number" name="prizesell"  required><br>
                                                        </div>

                                                    </div>
                                                    
                                                    </div>
                                                    <div class="set_manageadmin3">
                                                    <input type="submit" name="SubmitProduct" value="ยืนยัน" style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
                                                    </div>



                                                <!-- <div class="table_section">

                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Images</th>
                                                                <th>Name</th>
                                                                <th>Price</th>
                                                                <th>color</th>
                                                                <th>size</th>
                                                                <th>Count</th>
                                                                <th></th>
                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td><img src="images/shop/" alt="รูป"></td>
                                                                <td>TEST</td>
                                                                <td>500 THB</td>
                                                                <td>RED</td>
                                                                <td>XL</td>
                                                                <td>50</td>
                                                                <td><button
                                                                        style="margin: 5px 10px; background-color:#707070; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
                                                                        แก้ไข
                                                                    </button>
                                                                    <button
                                                                        style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff;cursor: pointer;">
                                                                        ลบ
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div> -->
                                                <div class="main_content_bestseller">
                                                            <div class="container-fluid p-0">
                                                                <div class="row justify-content-center">
                                                                    <div class="col-lg-12">
                                                                        <div class="single_element_bestseller">
                                                                            <div class="quick_activity_bestseller">
                                                                                <div class="row">
                                                                                    <div class="col-12">
                                                                                        <div class="quick_activity_wrap_bestseller">

                                                                                            <form action="">
                                                                                                <div class="single_quick_activity_bestseller">
                                                                                                    <div class="count_content_bestseller">
                                                                                                        <div class="test">
                                                                                                            <h3><span class="counter">Stock</span></h3>
                                                                                                            <!-- <div>
                                                                                                                <input type="date">
                                                                                                                <span> - </span>
                                                                                                                <input type="date">
                                                                                                            </div> -->
                                                                                                        </div>
                                                                                                        <hr>
                                                                                                        <div class="table_section">
                                                                                                            
                                                                                                            <table>
                                                                                                                <thead>
                                                                                                                    <tr>
                                                                                                                        <th>No</th>
                                                                                                                        <th>Images</th>
                                                                                                                        <th>Name</th>
                                                                                                                        <th>Price</th>
                                                                                                                        <th>color</th>
                                                                                                                        <th>size</th>
                                                                                                                        <th>Count</th>
                                                                                                                        <th></th>
                                                                                                                    </tr>
                                                                                                                </thead>
                                                                                                                <tbody>
                                                                                                                    <?php while($row = oci_fetch_array($inventory)){ ?>
                                                                                                                    <?php 
                                                                                                                        $amount = oci_parse($conn, 
                                                                                                                        "SELECT SUM(AMOUNT)
                                                                                                                        FROM PRODUCT_DETAIL
                                                                                                                        WHERE ID_PRODUCT = ".$row['PID']." AND 
                                                                                                                        ID_SIZE = ".$row['SID']." AND 
                                                                                                                        ID_COLOR = ".$row['CID']."");   
                                                                                                                        oci_execute($amount);
                                                                                                                        $a = oci_fetch_array($amount);
                                                                                                                    ?>
                                                                                                                    <tr>
                                                                                                                        <td><?= $invID++ ?></td>
                                                                                                                        <td><img src="../images/shop/<?= $row['PHOTO'] ?>" alt="รูป"></td>
                                                                                                                        <td><?= $row['PNAME']?></td>
                                                                                                                        <td><?= $row['PRICE']?> THB</td>
                                                                                                                        <td><?= $row['CNAME']?></td>
                                                                                                                        <td><?= $row['SNAME']?></td>
                                                                                                                        <td><?= $a['SUM(AMOUNT)'] ?></td>
                                                                                                                        <td><button
                                                                                                                                style="margin: 5px 10px; background-color:#707070; border: none; padding: 0px 8px; color: #fff; cursor: pointer;">
                                                                                                                                แก้ไข
                                                                                                                            </button>
                                                                                                                            <button
                                                                                                                                style="margin: 5px 10px; background-color:#c00000; border: none; padding: 0px 8px; color: #fff;cursor: pointer;">
                                                                                                                                ลบ
                                                                                                                            </button>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php } ?>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                    


                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button_perr">
            <div class="button_listr">
                <button
                    style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;"
                    onclick="openaddnamePro()">+</button>
            </div>
            <div class="button_listr1">
                <button
                    style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;"
                    onclick="openaddyeeho()">+</button>
            </div>
            <div class="button_listr2">
                <button
                    style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;"
                    onclick="openaddtype()">+</button>
            </div>
            <div class="button_listr3">
                <button
                    style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;"
                    onclick="openaddcolor()">+</button>
            </div>
            <div class="button_listr4">
                <button
                    style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;"
                    onclick="openaddseason()">+</button>
            </div>
        </div>

        <div class="button_perr1">
            <div class="button_listr5">
                <button
                    style="margin: 5px 10px; background-color:#00a015; border: none; padding: 0px 8px; color: #fff; cursor: pointer;"
                    onclick="openaddsizee()">+</button>
            </div>
        </div>
    </div>
</div>
<?php include 'detail_addproduct.php';?>

<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>
<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>

<?php 
if(isset($_POST['nameP'])){
    $countidp = oci_parse($conn, "SELECT NAME 
    FROM PRODUCT_NAME WHERE NAME ='".$_POST['namePN']."'");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    if(isset($row[0])){
    echo "<script>alert('ชื่อข้อมูลนี้ถูกใช้ไปแล้ว');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';
    }else{
    $countidp = oci_parse($conn, "SELECT count(*) 
    FROM PRODUCT_NAME");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    $idp = $row['COUNT(*)'];
    $idp++;
    $update = oci_parse($conn, 
    "INSERT INTO PRODUCT_NAME (ID,NAME)
                    VALUES ('".$idp."','".$_POST['namePN']."')");
    oci_execute($update);
    
    echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';

    }
}

if(isset($_POST['brandP'])){
    $countidp = oci_parse($conn, "SELECT NAME 
    FROM BRAND WHERE NAME ='".$_POST['brandPN']."'");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    if(isset($row[0])){
    echo "<script>alert('ชื่อข้อมูลนี้ถูกใช้ไปแล้ว');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';
    }else{
    $countidp = oci_parse($conn, "SELECT count(*) 
    FROM BRAND");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    $idp = $row['COUNT(*)'];
    $idp++;
    $update = oci_parse($conn, 
    "INSERT INTO BRAND (ID,NAME)
                    VALUES ('".$idp."','".$_POST['brandPN']."')");
    oci_execute($update);
    
    echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';

    }
}


if(isset($_POST['typeP'])){
    $countidp = oci_parse($conn, "SELECT NAME 
    FROM TYPE WHERE NAME ='".$_POST['typePN']."'");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    if(isset($row[0])){
    echo "<script>alert('ชื่อข้อมูลนี้ถูกใช้ไปแล้ว');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';
    }else{
    $countidp = oci_parse($conn, "SELECT count(*) 
    FROM TYPE");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    $idp = $row['COUNT(*)'];
    $idp++;
    $update = oci_parse($conn, 
    "INSERT INTO TYPE (ID,NAME)
                    VALUES ('".$idp."','".$_POST['typePN']."')");
    oci_execute($update);
    
    echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';

    }
}


if(isset($_POST['colorP'])){
    $countidp = oci_parse($conn, "SELECT NAME 
    FROM COLORTABLE WHERE NAME ='".$_POST['colorPN']."'");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    if(isset($row[0])){
    echo "<script>alert('ชื่อข้อมูลนี้ถูกใช้ไปแล้ว');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';
    }else{
    $countidp = oci_parse($conn, "SELECT count(*) 
    FROM COLORTABLE");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    $idp = $row['COUNT(*)'];
    $idp++;
    $update = oci_parse($conn, 
    "INSERT INTO COLORTABLE (ID,NAME,CODE)
                    VALUES ('".$idp."','".$_POST['colorPN']."','".$_POST['colorPC']."')");
    oci_execute($update);
    
    echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';

    }
}


if(isset($_POST['seasonP'])){
    $countidp = oci_parse($conn, "SELECT NAME 
    FROM SEASON WHERE NAME ='".$_POST['seasonPN']."'");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    if(isset($row[0])){
    echo "<script>alert('ชื่อข้อมูลนี้ถูกใช้ไปแล้ว');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';
    }else{
    $countidp = oci_parse($conn, "SELECT count(*) 
    FROM SEASON");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    $idp = $row['COUNT(*)'];
    $idp++;
    $update = oci_parse($conn, 
    "INSERT INTO SEASON (ID,NAME)
                    VALUES ('".$idp."','".$_POST['seasonPN']."')");
    oci_execute($update);
    
    echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';

    }
}

if(isset($_POST['sizeP'])){
    $countidp = oci_parse($conn, "SELECT NAME 
    FROM SIZETABLE WHERE NAME ='".$_POST['sizePN']."'");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    if(isset($row[0])){
    echo "<script>alert('ชื่อข้อมูลนี้ถูกใช้ไปแล้ว');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';
    }else{
    $countidp = oci_parse($conn, "SELECT count(*) 
    FROM SIZETABLE");
    oci_execute($countidp);
    $row = oci_fetch_array($countidp);
    $idp = $row['COUNT(*)'];
    $idp++;
    $update = oci_parse($conn, 
    "INSERT INTO SIZETABLE (ID,NAME)
                    VALUES ('".$idp."','".$_POST['sizePN']."')");
    oci_execute($update);
    
    echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
    echo '<script>window.location.href = "../admin/manageproduct.php";</script>';

    }
}

?>