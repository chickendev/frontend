<?php 
    include 'headadmin.php';
    include 'menu.php'; 
    
        
      
    $memberlist = oci_parse($conn, "SELECT 
    M_ID, 
    M_NAME, 
    M_LNAME, 
    M_ADDRESS, 
    M_TEL, 
    M_PURCHASE
    FROM MEMBERS
    ORDER BY M_ID ASC
    ");
    ociexecute($memberlist);
    $mid = 1;
?>

<?php  include 'header.php'; ?>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <form class="form-inline">
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>
                    </form>
                <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>name</th>
                                                <th>Lname</th>
                                                <th>Address</th>
                                                <th>Tel</th>
                                                <th>Purchase</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php while($mlist = oci_fetch_array($memberlist)){?>   
                                        <tr>
                                            <td><?= $mlist['M_ID'] ?></td>
                                            <td><?= $mlist['M_NAME'] ?></td>
                                            <td><?= $mlist['M_LNAME'] ?></td>
                                            <td><?= $mlist['M_ADDRESS'] ?></td>
                                            <td><?= $mlist['M_TEL'] ?></td>
                                            <td><?= $mlist['M_PURCHASE'] ?></td>
                                        </tr>
                                    <?php  } ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>        