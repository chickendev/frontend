<?php include 'head.php'; ?>

<!-- Header -->

<header><?php 
session_start();
if($_SESSION['success']=='OK'){
    include 'headeraf.php';
}else{
    include 'header.php'; 
}
 ?></header>


<script>
    function checkout(sum) {
        var cash = document.getElementById("money").value;


        if (cash == sum) {
            document.location = "../../api/stock.php ";
        } else if (cash > sum) {
            alert("กรุณาโอนเงินให้พอดีกับยอดสั่งซื้อ");
        } else {
            alert("จำนวนเงินไม่พอ");
            //จำนวนเงินไม่พอ
        }

    }
    
</script>
<?php
    $query = "SELECT * FROM MEMBERS WHERE M_ID = ".$_SESSION['iduser'];
    $result = oci_parse($conn, $query);
    oci_execute($result);
    while (oci_fetch_array($result)) {
        $_SESSION['purchase'] =  oci_result($result, 'M_PURCHASE');
    }

    foreach($_SESSION['cart'] as $key => $value){
        $_SESSION['cart'][$key]['alreadydiscount'] = 'NOTOK';
    }

?>
<div class="main_content_detail">
    <div class="container-fluid p-0">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="single_element_detail">
                    <div class="quick_activity_detail">
                        <div class="row">
                            <div class="col-12">
                                <div class="quick_activity_wrap_detail">

                                    <form action="">
                                        <div class="single_quick_activity_detail">
                                            <div class="count_content_detail">
                                                <div class="test">
                                                    <h3><span class="counter">Sold out List</span></h3>
                                                </div>


                                                <hr>
                                                <div class="table_section">
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Name</th>
                                                                <th>Color</th>
                                                                <th>Size</th>
                                                                <th>Price</th>
                                                                <th>Count</th>
                                                                <th>Total price</th>
                                                                <th>Discount</th>
                                                                <th>Total discount</th>
                                                                <th>Include discount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        

                                                            <?php
                                                                $idlist = 1; 
                                                                foreach($_SESSION['cart'] as $key => $value){
                                                                    $_SESSION['cart'][$key]['idlist'] = $idlist;
                                                            ?>
                                                            <tr>
                                                                <td><?=  $idlist++ ?></td>
                                                                <td><?= $value['pname'] ?></td>
                                                                <td><?= $value['color'] ?></td>
                                                                <td><?= $value['size'] ?></td>
                                                                <td><?= $value['price'] ?> THB</td>
                                                                <td><?= $value['qty'] ?></td>
                                                                <td><?= $value['price']*$value['qty'] ?> THB</td>
                                                                <td><?= $value['discount'] ?> %</td>
                                                                <td><?= $value['per_discount'] ?> THB</td>
                                                                <td><?= $value['include_discount'] ?> THB</td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="priceall">
    <h1>ราคารวมทั้งหมด</h1>
    <?php $sum = 0;
     foreach($_SESSION['cart'] as $key => $value){
        if($value['include_discount'] != '0'){
            $sum += $value['include_discount']; 
        }else{
            $sum += $value['price']*$value['qty'];
        }
        
    }
    foreach($_SESSION['cart'] as $key => $value){
        $_SESSION['cart'][$key]['total_'] = $value['price']*$value['qty'] ;
    }
    $_SESSION['total'] = $sum ;
    
    ?>
    <h2><?= $sum ?> THB</h2>
</div>

    <div class="price_cf">
        <div>
            <input id="money" type="text" placeholder="กรอกจำนวนเงิน" style="font-size: 18px;">
        </div>
        <div>
            <button onclick="checkout(<?php echo $sum?>)" style="width: 100px; border: 1px solid black;">ยืนยัน</button>
        </div>
        
    </div>
<?php

echo '<pre>';
print_r($_SESSION['cart']);
print_r($_SESSION['purchase']);
echo '</pre>';

?>
<!-- <?php
// echo '<pre>';
// print_r($_SESSION['cart']);
// echo '</pre>';
?> -->

<?php include 'footer.php'; ?>