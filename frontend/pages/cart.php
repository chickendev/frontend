<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../images/logo/logo1.png">
    <link rel="icon" type="image/png" href="../images/logo/logo1.png" />
    <title>PEN DO RIE</title>
    <link rel="stylesheet" type="text/css" href="../css/cart.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css" />
    <!--bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous">
    </script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <?php

        require_once('../../api/db.php');
        
        $noofbox = oci_parse($conn, "SELECT PRODUCT.ID,NAME FROM PRODUCT,PRODUCT_NAME WHERE ID_PD_NAME = PRODUCT_NAME.ID");
        oci_execute($noofbox);


        session_start();
        if(isset($_SESSION['nomore'])){
            echo '<script>
                alert("สินค้าไม่พอ");
            </script>';
            unset($_SESSION['nomore']);
        }
    
        if($_SESSION['success'] == "OK"){
            header("location: cartaf.php");
        }



        // echo '<pre>';
        //     print_r($_SESSION['cart']);
        // echo '</pre>';



        $sum = 0;
        // echo '<pre>';
        //session_unset();
        // foreach($_SESSION['cart'] as $key => $value){
        //     print_r($key."=>");
        
        //     print_r($value) ;
        // }  
        // echo '</pre>';
        
    ?>

    <script>
    function plus(id) {
        document.location = "../../api/cart.php?act=plus&pid=" + id;
    }

    function minus(id) {
        document.location = "../../api/cart.php?act=minus&pid=" + id;
    }

    function removeitem(key) {
        document.location = "../../api/cart.php?act=remove&item="+key;
    }


    // function checkout(sum) {
    //     var cash = document.getElementById("money").value;


    //     if (cash == sum) {
    //         document.location = "../../api/stock.php ";
    //     } else if (cash > sum) {
    //         alert("กรุณาโอนเงินให้พอดีกับยอดสั่งซื้อ");
    //     } else {
    //         alert("จำนวนเงินไม่พอ");
    //         //จำนวนเงินไม่พอ
    //     }

    // }
    </script>
    
</head>

<body>






    <!-- Header -->


    <header><?php include 'header.php'; ?></header>
    <div class="container-cart-item">
        <?php if(!empty($_SESSION['cart'])){ ?>
        <H1>ตะกร้า (<?php echo count($_SESSION['cart']); ?> ชิ้น)</H1>
        <?php }else{?>
        <H1>ตะกร้า ( 0 ชิ้น) </H1>
        <?php }?>
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12 col-11 mx-auto">
                    <div class="row mt-5 gx-3">
                        <!-- left side div -->

                        <?php if(!empty($_SESSION['cart'])){ ?>
                        <?php foreach($_SESSION['cart'] as $key => $value) {?>

                        <div class="col-md-12 col-lg-8 col-11 main_cart mb-lg-0 mb-5 shadow">
                            <div class="card p-4">
                                <!-- <h2 class = "py-4 font-weight-bold">Cart (2 item)</h2> -->
                                <div class="row">
                                    <!--cart img-->
                                    <div
                                        class="col-md-5 col-11 mx-auto bg-light d-flex justify-content-center align-items-center shadow product_img">
                                        <img src="../images/shop/kkk.png" class="img-fluid">
                                    </div>
                                    <!--cart product detail-->
                                    <div class="col-md-7 col-11 mx-auto px-4 mt-2">
                                        <div class="row">
                                            <!--product name-->
                                            <div class="col-6 card-title">
                                                <h2 class="mb-4 product_name"><?=$value['pname'] ?></h2>
                                                <p class="mb-2">COLOR: <span><?=$value['color'] ?></span></p>
                                                <p class="mb-2">SIZE: <?=$value['size'] ?></p>
                                            </div>

                                            <!--quantity inc dec-->
                                            <div class="col-6">
                                                <ul class="pagination justify-content-end set_quantity">
                                                    <li class="page-item">
                                                        <!-- <button class="pafe-link"
                                                                            onclick="decreaseNumber('textbox','')"> <i
                                                                                class="fa-solid fa-minus"></i> </button> -->
                                                        <button onclick="minus(<?php echo $key; ?>)" name="plus"
                                                            class="pafe-link ml-2"> <i class="fa-solid fa-minus"
                                                                id="plus<?php echo $key; ?>"></i> </button>
                                                    </li>
                                                    <li class="page-item"> <input type="text" name="" class="pafe-link"
                                                            value="<?php echo $value['qty']?>"
                                                            id="textbox<?php echo $key;?>"></input> </li>
                                                    <li class="page-item">
                                                        <!-- <button class="pafe-link"
                                                                            onclick=""> <i
                                                                                class="fa-solid fa-plus"></i> </button> -->


                                                        <button onclick="plus(<?php echo $key; ?>)" name="plus"
                                                            class="pafe-link mr-2"> <i class="fa-solid fa-plus"
                                                                id="plus<?php echo $key; ?>"></i> </button>

                                                    </li>

                                                </ul>
                                            </div>

                                        </div>

                                        <!--remove and price-->
                                        <div class="row">
                                            <div  class="col-8 d-flex justify-content-between remove_wish">
                                                <p onclick="removeitem(<?= $key ?>)" style="cursor: pointer;"><i class="fas fa-trash-alt"></i> REMOVE ITEM</p>
                                            </div>
                                            <div class="col-4 d-flex justify-content-end price_money">
                                                <h3><span id="itemval"><?=$value['price']*$value['qty'] ?></span> THB
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php  } ?>
                        <?php }?>
                        <!--end-->

                        <!--right side div-->
                        <div class="col-md-12 col-lg-4 col-11 mx-auto mt-lg-0 mt-md5">
                            <div class="pight_side p-3 show bg-white">
                                <h2 class="product_name mb-5">The Total Amount Of</h2>
                                <div class="price_indiv d-flex justify-content-between">
                                    <p>Product amount</p>
                                    <p><span id="product_total_amt">

                                            <?php 
                                                if(!empty($_SESSION['cart'])){
                                                    foreach($_SESSION['cart'] as $key => $value){
                                                        $sum += $value['price']*$value['qty'];
                                                    } 
                                                    echo $sum;
                                                    $_SESSION['sum'] = $sum;
                                                }else{
                                                   echo 0 ;
                                                }
                                            ?>

                                        </span> THB</p>
                                </div>
                                <hr>
                                <!-- <input id="money" type="text" class="mb-2"> -->
                                <!-- <div class="total-amt d-flex justify-content-between font-weight-bold">
                                            <p>The total amount of (including VAT)</p>
                                            <p><span id="total_cart_amt">0.00</span> THB</p>                          ----------onclick="checkout(<//?php echo $_SESSION['sum'] ;?>)"
                                        </div> -->
                                <button onclick="openCheckout();"
                                    class="btn btn-primary text-uppercase mb-2">Checkout</button>
                                <form action="../../api/cart.php" method="post">
                                    <input type="submit" class="btn btn-primary text-uppercase" name="clear"
                                        value="clear">
                                </form>
                            </div>

                            <!-- discount code part -->
                            <div class="discount_code mt-3 shadow">
                                <div class="card">
                                    <div class="card-body">
                                        <a class="d-flex justify-content-between" data-toggle="collapse"
                                            href="#collapseExample" aria-expanded="false"
                                            aria-controls="collapseExample">
                                            Add a discount code (optional)
                                            <span><i class="fas fa-chevron-down pt-1"></i></span>
                                        </a>
                                        <div class="collapse" id="collapseExample">
                                            <div class="mt-3">
                                                <input type="text" name="" id="discount_code1"
                                                    class="form-control font-weight-bold"
                                                    placeholder="Enter the discount code">
                                                <small id="error_trw" class="text-dark mt-3">code is thapa</small>
                                            </div>
                                            <button class="btn btn-primary btn-sm mt-3"
                                                onclick="discount_code()">Apply</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            


                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>


    <div class="modall" id="modalcheck_out" style="display: none;">
        <div class="modall-page">
            <span>จะดำเนินการต่อโดยไม่เข้าสู่ระบบหรือไม่</span><br>
            <button onclick="openCheckout_yes()">Yes</button><button onclick="closeModal2()">No</button>
        </div>
    </div>


    <form action="../../api/bill.php" method="POST">
        <div class="modall" id="modalcheck_out_yes" style="display: none;">
            <div class="modall-page">
                <div class="mr-4">
                    <span>ชื่อ : </span><br><input type="text" name="cusname" required><br>
                </div>
                <div class="mr-4">
                    <span>นามสกุล : </span><br><input type="text" name="cuslname" required><br>
                </div>
                <div class="mr-4">
                    <span>เบอร์โทร : </span><br><input type="text" name="custel" required><br>
                </div>
                <div class="mr-4">
                    <span>ที่อยู่ : </span><br><textarea type="text" name="cusad" required></textarea><br>
                </div>
                <!-- onclick="window.location='billing.php'" -->
                <input type="submit" value="OK">
                <!-- <a href="billing.php"></a> -->
            </div>
        </div>
    </form>
    </div>
    </div>





    <script src="../js/cart.js"></script>
    <script src="../js/details.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
</body>