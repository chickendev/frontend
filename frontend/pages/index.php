<?php
session_start();
    $_SESSION['error'] = "";
    if($_SESSION['success'] == "OK"){
        header("location: afterlogin.php");
    }else{
        $_SESSION['success'] = "NOTOK";
    }
include 'head.php'; ?>

<!-- Header -->
<header><?php include 'header.php'; ?></header>

<!-- Slide -->
<?php include 'slide.php'; ?>


<!-- itemshop -->
<div class="container-lish-item">
    <?php while ($row = oci_fetch_array($noofbox)) {
        echo '<div id = "productlist" class="product-item" >';
        $size = oci_parse($conn, "SELECT NAME FROM PRODUCT_LIST,SIZETABLE WHERE PRODUCT_LIST.ID_SIZE = SIZETABLE.ID AND ID_PRODUCT = " . $row['ID'] . " GROUP BY NAME,ID ORDER BY ID ASC");
        oci_execute($size);

        $color = oci_parse($conn, "SELECT ID_COLOR FROM PRODUCT_LIST WHERE ID_PRODUCT =  " . $row['ID'] . "GROUP BY ID_COLOR ORDER BY ID_PRODUCT ASC");
        oci_execute($color);

        

        $price = oci_parse($conn, "SELECT PRICE FROM PRODUCT_LIST WHERE ID_PRODUCT =" . $row['ID']);
        oci_execute($price);

        $setprice = oci_parse($conn, "SELECT PRICE FROM PRODUCT_LIST WHERE ID_PRODUCT =" . $row['ID']);
        oci_execute($setprice);
        oci_fetch_array($setprice);
        $setPrice = oci_result($setprice, 'PRICE');

        $setphoto = oci_parse($conn, "SELECT PHOTO FROM PRODUCT_LIST WHERE ID_PRODUCT =" . $row['ID']);
        oci_execute($setphoto);
        oci_fetch_array($setphoto);
        $setphoto = oci_result($setphoto, 'PHOTO');


    ?>
        <div class="card">
            <div class="imgBx">
            <img class="product-img" src="../images/shop/<?php echo $setphoto?>" alt="ม่ายมีรูป">

            </div>
            <div class="contentBx">
                <h2><?php echo $row['NAME'] ?></h2>
                <h4 id="price-item<?php echo $row['ID'] ?>"><?php echo $setPrice . ' ' . 'THB'; ?></h4>
                <div class="size">
                    <h3>Size :</h3>
                    <?php
                    while ($rowsize = oci_fetch_array($size)) {

                        //$sizeN = oci_result($size, 'NAME');
                        $rp = oci_fetch_array($price);
                        echo '<span class ="botton'.$row['ID'].'" onclick="showPrice('. $row['ID'].','.$rp['PRICE'].',\''.$rowsize['NAME'].'\')" id="bg-'.$rowsize['NAME'].''.$row['ID'].'">'. $rowsize['NAME'] .'</span>';


                    }
                    ?>
                </div>
                <div class="color">
                    <h3>Color :</h3>
                    <?php while ($rowcolor = oci_fetch_array($color)) { 
                        $CodeC = oci_parse($conn,"SELECT CODE FROM COLORTABLE WHERE COLORTABLE.ID = ".$rowcolor['ID_COLOR']." ORDER BY COLORTABLE.ID ASC");
                        oci_execute($CodeC);
                        $CodeColer = oci_fetch_array($CodeC);
                        //$setcode = oci_result($CodeC, 'CODE');                       
                        ?>

                        <span style=<?php echo 'background:' . $CodeColer['CODE']; ?>></span>
                        
                    <?php } ?>

                </div>
                <a  onclick="openProductDetail(<?php echo $row['ID'] ?>)" style="cursor: pointer;">Details</a>
            </div>
        </div>
<?php echo '</div>'; ?>

        <!-- Detail -->
        <div id="modalDesc<?php echo $row['ID'] ?>" class="modal" style="display: none;">
            <?php include 'detail.php'; ?>
        </div>
    <?php } ?>

    </body>
    <?php include 'footer.php'; ?>
    