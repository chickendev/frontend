<!DOCTYPE html>
<html lang="en">
<?php

session_start();

    
    if($_SESSION['success'] == 'OK' && $_SESSION['emp'] == 'emp' ){
      header("location: ../admin/indexadmin.php");
    }
    else if($_SESSION['success'] == 'OK'){
      header("location: ../profile.php");
    }
    if($_SESSION['error'] == 'true'){
      echo '<script> alert("Wrong Username or Password!"); </script>'; 
      unset($_SESSION['error']);
    }
    

?>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>LOGIN</title>
</head>

<body>
  <div class="title-center">
    <h1>LOGIN</h1>

    <form action="../../../api/login_db.php" method="post">
      <div class="txt_field">
        <input name="username" type="text" required>
        <span></span>
        <label>Username</label>
      </div>
      <div class="txt_field">
        <input name="password" type="password" required>
        <span></span>
        <label>Password</label>
      </div>
      <div class="pass">
        <a href="forgot.php">Forgot Password?</a>  
      </div>
      <input name="login_user" type="submit" value="Login">
      <div class="signup_link">
        Not a member? <a href="register.php">Signup</a>
      </div>
      <div class="back_page">
        <a href="../index.php">Back</a>
      </div>
    </form>
  </div>

</body>
<style>
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Kanit', sans-serif;
  }

  body {
    margin: 0;
    padding: 0;
    background: linear-gradient(white);
    height: 100vh;
    overflow: hidden;
  }

  .title-center {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 400px;
    background: white;
    border-radius: 10px;
    box-shadow: 10px 10px 15px rgba(0, 0, 0, 0.05);
  }

  .title-center h1 {
    text-align: center;
    padding: 20px 0;
    border-bottom: 1px solid silver;
  }

  .title-center form {
    padding: 0 40px;
    box-sizing: border-box;
  }

  form .txt_field {
    position: relative;
    border-bottom: 2px solid #adadad;
    margin: 30px 0;
  }

  .txt_field input {
    width: 100%;
    padding: 0 5px;
    height: 40px;
    font-size: 16px;
    border: none;
    background: none;
    outline: none;
  }

  .txt_field label {
    position: absolute;
    top: 50%;
    left: 5px;
    color: #adadad;
    transform: translateY(-50%);
    font-size: 16px;
    pointer-events: none;
    transition: .5s;
  }

  .txt_field span::before {
    content: '';
    position: absolute;
    top: 40px;
    left: 0;
    width: 0%;
    height: 2px;
    background: #035668;
    transition: .5s;
  }

  .txt_field input:focus~label,
  .txt_field input:valid~label {
    top: -5px;
    color: #035668;
  }

  .txt_field input:focus~span::before,
  .txt_field input:valid~span::before {
    width: 100%;
    
  }

  .pass {
    margin: -5px 0 20px 5px;
    cursor: pointer;
    
  }

  .pass a {
    color: #adadad;
    text-decoration: none;
  }

  .pass:hover {
    text-decoration: underline;
  }

  input[type="submit"] {
    width: 100%;
    height: 50px;
    border: 1px solid;
    background: #035668;
    border-radius: 25px;
    font-size: 18px;
    color: white;
    font-weight: 700;
    cursor: pointer;
    outline: none;
  }

  input[type="submit"]:hover {
    border-color: #035668;
    transition: .5s;
  }

  .signup_link {
    margin: 30px 0;
    text-align: center;
    font-size: 16px;
    color: #666666;
  }

  .signup_link a {
    color: #2691d9;
    text-decoration: none;
  }

  .signup_link a:hover {
    text-decoration: underline;
  }

  .back_page {
    margin: 30px 0;
    text-align: center;
    font-size: 16px;
    font-weight: 700;
    color: #0000;
  }

  .back_page a {
    color: #2691d9;
    text-decoration: none;
    color: #000;
  }

  .back_page a:hover {
    text-decoration: underline;
    text-decoration: none;
  }
</style>

</html>